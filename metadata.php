<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Controller\Admin\CategoryMain as OxCategoryMain;
use OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax as OxListComponentAjax;
use OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration as OxModuleConfiguration;
use OxidEsales\Eshop\Application\Controller\Admin\UserList as OxUserList;
use OxidEsales\Eshop\Application\Controller\RssController as OxRssController;
use OxidEsales\Eshop\Application\Model\ArticleList as OxArticleList;
use OxidEsales\Eshop\Application\Model\CategoryList as OxCategoryList;
use OxidEsales\Eshop\Application\Model\ContentList as OxContentList;
use OxidEsales\Eshop\Application\Model\Delivery as OxDelivery;
use OxidEsales\Eshop\Application\Model\DeliveryList as OxDeliveryList;
use OxidEsales\Eshop\Application\Model\DeliverySet as OxDeliverySet;
use OxidEsales\Eshop\Application\Model\ManufacturerList as OxManufacturerList;
use OxidEsales\Eshop\Application\Model\RssFeed as OxRssFeed;
use OxidEsales\Eshop\Application\Model\Shop as OxShop;
use OxidEsales\Eshop\Application\Model\VendorList as OxVendorList;
use OxidEsales\Eshop\Core\Output as OxOutput;
use OxidEsales\Eshop\Core\ShopControl as OxShopControl;
use OxidEsales\Eshop\Core\ViewConfig as OxViewConfig;
use OxidEsales\Eshop\Core\WidgetControl as OxWidgetControl;
use TheRealWorld\ToolsModule\Application\Controller\Admin\CategoryMain;
use TheRealWorld\ToolsModule\Application\Controller\Admin\ListComponentAjax;
use TheRealWorld\ToolsModule\Application\Controller\Admin\ModuleConfiguration;
use TheRealWorld\ToolsModule\Application\Controller\Admin\UserList;
use TheRealWorld\ToolsModule\Application\Controller\RssController;
use TheRealWorld\ToolsModule\Application\Model\ArticleList;
use TheRealWorld\ToolsModule\Application\Model\CategoryList;
use TheRealWorld\ToolsModule\Application\Model\ContentList;
use TheRealWorld\ToolsModule\Application\Model\Delivery;
use TheRealWorld\ToolsModule\Application\Model\DeliveryList;
use TheRealWorld\ToolsModule\Application\Model\DeliverySet;
use TheRealWorld\ToolsModule\Application\Model\ManufacturerList;
use TheRealWorld\ToolsModule\Application\Model\RssFeed;
use TheRealWorld\ToolsModule\Application\Model\Shop;
use TheRealWorld\ToolsModule\Application\Model\VendorList;
use TheRealWorld\ToolsModule\Core\Output;
use TheRealWorld\ToolsModule\Core\ShopControl;
use TheRealWorld\ToolsModule\Core\ViewConfig;
use TheRealWorld\ToolsModule\Core\WidgetControl;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwtools',
    'title' => [
        'de' => 'the-real-world - Werkzeuge',
        'en' => 'the-real-world - Tools',
    ],
    'description' => [
        'de' => 'Die Werkzeugsammlung ermöglicht u.a. in Templates den direkten Zugriff auf die Config und den Request und sie stellt weitere nützliche Funktionen anderen Modulen zur Verfügung.',
        'en' => 'The toolkit allows i.a. the template direct access to the Config and the request and provides other useful functions for other modules.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwtools'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\ToolsModule\Core\ToolsEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\ToolsModule\Core\ToolsEvents::onDeactivate',
    ],
    'extend' => [
        // Core
        OxViewConfig::class    => ViewConfig::class,
        OxShopControl::class   => ShopControl::class,
        OxWidgetControl::class => WidgetControl::class,
        OxOutput::class        => Output::class,
        // Model
        OxArticleList::class      => ArticleList::class,
        OxCategoryList::class     => CategoryList::class,
        OxContentList::class      => ContentList::class,
        OxDelivery::class         => Delivery::class,
        OxDeliveryList::class     => DeliveryList::class,
        OxDeliverySet::class      => DeliverySet::class,
        OxManufacturerList::class => ManufacturerList::class,
        OxRssFeed::class          => RssFeed::class,
        OxShop::class             => Shop::class,
        OxVendorList::class       => VendorList::class,
        // Controller\Frontend
        OxRssController::class => RssController::class,
        // Controller\Backend
        OxListComponentAjax::class   => ListComponentAjax::class,
        OxCategoryMain::class        => CategoryMain::class,
        OxModuleConfiguration::class => ModuleConfiguration::class,
        OxUserList::class            => UserList::class,
    ],
    'blocks' => [
        [
            'template' => 'module_config.tpl',
            'block'    => 'admin_module_config_var',
            'file'     => 'Application/views/blocks/admin_module_config_var.tpl',
        ],
        [
            'template' => 'module_config.tpl',
            'block'    => 'admin_module_config_var_type_str',
            'file'     => 'Application/views/blocks/admin_module_config_var_type_str.tpl',
        ],
        [
            'template' => 'bottomnaviitem.tpl',
            'block'    => 'admin_bottomnaviitem',
            'file'     => 'Application/views/blocks/admin_bottomnaviitem.tpl',
        ],
    ],
    'smartyPluginDirectories' => [
        'Smarty/AddHyphensToISBN',
    ],
    'settings' => [
        [
            'group' => 'trwtoolslog',
            'name'  => 'iTRWToolsLogLoadMaxEntries',
            'type'  => 'num',
            'value' => 200,
        ],
        [
            'group' => 'trwtoolslog',
            'name'  => 'sTRWToolsLogMailTo',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group' => 'trwtoolslog',
            'name'  => 'bTRWToolsLogWriteToDb',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwtoolsexception',
            'name'  => 'bTRWToolsExceptionLogRSSFeedActive',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwtoolsexception',
            'name'  => 'sTRWToolsExceptionLogRSSFeedToken',
            'type'  => 'str',
            'value' => '',
        ],
        [
            'group' => 'trwtoolsexception',
            'name'  => 'iTRWToolsExceptionLogRSSFeedMaxEntries',
            'type'  => 'num',
            'value' => 5,
        ],
        [
            'group' => 'trwtoolsfileoptions',
            'name'  => 'iTRWToolsFileAgeForImport',
            'type'  => 'num',
            'value' => 360,
        ],
        [
            'group' => 'trwtoolspopupfilter',
            'name'  => 'bTRWToolsSearchContaining',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwtoolsphoneticoptions',
            'name'  => 'bTRWToolsUseColognePhonetic',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwtoolsimg',
            'name'  => 'bTRWToolsUseToolsDynamicImageGenerator',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwtoolsuser',
            'name'  => 'bTRWToolsDontShowGuestUserInBackendList',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwtoolsheader',
            'name'  => 'bTRWToolsSetSameOriginHeader',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwtoolsloglevel',
            'name'  => 'bTRWToolsLoglevelOverride',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group'       => 'trwtoolsloglevel',
            'name'        => 'sTRWToolsLoglevelOverrideDefault',
            'type'        => 'select',
            'value'       => 'default',
            'constraints' => 'default|all|all_no_notice|all_no_noticewarning|all_no_noticewarningdeprecated|just_errors|flexible',
        ],
        [
            'group' => 'trwtoolsloglevel',
            'name'  => 'sTRWToolsLoglevelOverrideFlex',
            'type'  => 'str',
            'value' => '',
        ],
        // These options are invisible because the "group" option is null
        [
            'group' => null,
            'name'  => 'aTRWToolsDynImgConfParamToPath',
            'type'  => 'aarr',
            'value' => [],
        ],
        [
            'group' => null,
            'name'  => 'aTRWToolsDynImgDetectWithTwoOptions',
            'type'  => 'arr',
            'value' => [],
        ],
    ],
];
