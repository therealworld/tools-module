<?php

/**
* @author  Mario Lorenz, www.the-real-world.de
* @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\ToolsModule\Vendor\Monolog;

use Exception;
use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use OxidEsales\Eshop\Core\Model\BaseModel;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
* This class is a handler for Monolog, which can be used
* to write records in a MySQL table created by OXID
*
* Class OxidDBHandler
*/
class OxidDBHandler extends AbstractProcessingHandler
{
    /**
    * @var string the table to store the logs in
    */
    private string $table = 'trwtoolsmonolog';

    /**
    * Constructor of this class, calls parent constructor
    *
    * @param bool|int $level Debug level which this handler should store
    * @param bool $bubble
    */
    public function __construct(
        $level = Logger::DEBUG,
        $bubble = true
    ) {
        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param array $aLogRecord
     * @throws Exception
     */
    protected function write(array $aLogRecord): void
    {
        $oMonolog = oxNew(BaseModel::class);
        $oMonolog->init($this->table);

        $aParams = [
            'oxchannel'  => $aLogRecord['channel'],
            'oxlevel'    => $aLogRecord['level'],
            'oxmessage'  => $aLogRecord['message'],
            'oxdatetime' => $aLogRecord['datetime']->format('U.u')
        ];

        $aParams = ToolsDB::convertDB2OxParams($aParams, $this->table);

        $oMonolog->assign($aParams);
        $oMonolog->save();
    }

    /** clean the log */
    public function reset(): void
    {
        $sTruncateSql = "truncate table " . $this->table;
        ToolsDB::execute($sTruncateSql);
    }
}
