<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwtoolslog'             => 'Log-Optionen',
    'SHOP_MODULE_GROUP_trwtoolsfileoptions'     => 'Datei-Optionen',
    'SHOP_MODULE_GROUP_trwtoolspopupfilter'     => 'Popup-Filteroptionen',
    'SHOP_MODULE_GROUP_trwtoolsphoneticoptions' => 'Kölner Phonetik',
    'SHOP_MODULE_GROUP_trwtoolsimg'             => 'Bild-Optionen',
    'SHOP_MODULE_GROUP_trwtoolsuser'            => 'Benutzer/Personen-Optionen',
    'SHOP_MODULE_GROUP_trwtoolsexception'       => 'RSS-Feed für Exception Log',
    'SHOP_MODULE_GROUP_trwtoolsheader'          => 'zusätzliche Header',
    'SHOP_MODULE_GROUP_trwtoolsloglevel'        => 'PHP LogLevel',

    'SHOP_MODULE_iTRWToolsLogLoadMaxEntries'              => 'Wieviel Log-Einträge sollen im Fall einer Betrachtung geladen werden?',
    'SHOP_MODULE_sTRWToolsLogMailTo'                      => 'Mailadresse für Logeinträge die zusätzlich als Mail versendet werden können.',
    'SHOP_MODULE_bTRWToolsLogWriteToDb'                   => 'Sollen Log-Einträge in DB gespeichert werden?',
    'SHOP_MODULE_bTRWToolsExceptionLogRSSFeedActive'      => 'RSS-Feed des Exception-Log aktivieren?',
    'SHOP_MODULE_sTRWToolsExceptionLogRSSFeedToken'       => 'Zugriffs-Token für RSS-Feed des Exception-Log',
    'SHOP_MODULE_iTRWToolsExceptionLogRSSFeedMaxEntries'  => 'Maximale Anzahl der Feed-Einträge',
    'SHOP_MODULE_iTRWToolsFileAgeForImport'               => 'Benötiges Alter einer Importdatei in Sekunden. Hintergrund: Nur die Dateien, die dieses Alter haben, gelten als vollständig hochgeladen.',
    'SHOP_MODULE_bTRWToolsSearchContaining'               => 'Statt der Filtersuche "String beginnt mit", "String enthält" in allen PopUp-Filtern nutzen. !Suchstring benötigt dann mindestens 3 Zeichen!',
    'SHOP_MODULE_bTRWToolsUseColognePhonetic'             => 'Wenn phonetische Suche gebraucht wird und möglich ist, diese auch nutzen?',
    'SHOP_MODULE_bTRWToolsUseToolsDynamicImageGenerator'  => 'Nutze den erweiterbaren dynamischen Bild-Generator',
    'SHOP_MODULE_bTRWToolsDontShowGuestUserInBackendList' => 'Zeige keine Gast-Kunden in der Kunden-Backend-Liste',
    'SHOP_MODULE_bTRWToolsSetSameOriginHeader'            => 'Setze SameOrigin-Header',
    'HELP_SHOP_MODULE_bTRWToolsSetSameOriginHeader'       => 'Die Same-Origin-Policy (SOP; deutsch "Gleiche-Herkunft-Richtlinie") ist ein Sicherheitskonzept, das clientseitigen Skriptsprachen wie JavaScript, aber auch CSS untersagt, auf Objekte (zum Beispiel Grafiken) zuzugreifen, die von einer anderen Webseite stammen oder deren Speicherort nicht der Origin entspricht. Sie stellt ein wesentliches Sicherheitselement in allen modernen Browsern und Webanwendungen zum Schutz vor Angriffen dar.',

    'SHOP_MODULE_bTRWToolsLoglevelOverride'                                       => 'Log Level übersteuern?',
    'HELP_SHOP_MODULE_bTRWToolsLoglevelOverride'                                  => 'Seit PHP 8 werden auch standardmäßig Warnings im Error-Log protokolliert. Je nach Webserver-Konfiguration ist log_errors in der Standardkonfiguration aktiv. Das führt dazu dass das Logfile entsprechend "gefüllt" wird. Hiermit kann das Verhalten übersteuert werden.',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault'                                => 'Log Level Optionen',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_default'                        => 'OXID Standard',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all'                            => 'Alles',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all_no_notice'                  => 'Alles außer "notice"',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all_no_noticewarning'           => 'Alles außer "notice" & "warning"',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all_no_noticewarningdeprecated' => 'Alles außer "notice" & "warning" & "deprecated"',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_just_errors'                    => 'Nur Fehler (ERROR)',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_flexible'                       => 'Eigene Einstellung',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideFlex'                                   => 'Konkrekte eigene Einstellung',
];
