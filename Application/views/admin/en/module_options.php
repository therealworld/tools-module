<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwtoolslog'             => 'Log-Options',
    'SHOP_MODULE_GROUP_trwtoolsfileoptions'     => 'Fileoptiones',
    'SHOP_MODULE_GROUP_trwtoolspopupfilter'     => 'Popup-Filteroptiones',
    'SHOP_MODULE_GROUP_trwtoolsphoneticoptions' => 'Cologne Phonetic',
    'SHOP_MODULE_GROUP_trwtoolsimg'             => 'Image-Options',
    'SHOP_MODULE_GROUP_trwtoolsuser'            => 'User-Options',
    'SHOP_MODULE_GROUP_trwtoolsexception'       => 'RSS-Feed for Exception Log',
    'SHOP_MODULE_GROUP_trwtoolsheader'          => 'additional Headers',
    'SHOP_MODULE_GROUP_trwtoolsloglevel'        => 'PHP LogLevel',

    'SHOP_MODULE_iTRWToolsLogLoadMaxEntries'              => 'How many log entries are to be loaded in the case of a viewing?',
    'SHOP_MODULE_sTRWToolsLogMailTo'                      => 'Mail address for log entries that can also be sent as email',
    'SHOP_MODULE_bTRWToolsLogWriteToDb'                   => 'store log entries in DB?',
    'SHOP_MODULE_bTRWToolsExceptionLogRSSFeedActive'      => 'activate RSS-Feed of Exception-Log?',
    'SHOP_MODULE_sTRWToolsExceptionLogRSSFeedToken'       => 'Token for RSS-Feed of Exception-Log',
    'SHOP_MODULE_iTRWToolsExceptionLogRSSFeedMaxEntries'  => 'How many log entries are to be loaded in the Feed?',
    'SHOP_MODULE_iTRWToolsFileAgeForImport'               => 'needed age of importfiles in seconds. Background: only the files with this age are complete uploaded.',
    'SHOP_MODULE_bTRWToolsSearchContaining'               => 'In all pop-up filters, instead of the filter search "String starts with", the filter search uses "String contains". !Search string required at least 3 characters!',
    'SHOP_MODULE_bTRWToolsUseColognePhonetic'             => 'If phonetic search is needed and possible to also use them?',
    'SHOP_MODULE_bTRWToolsUseToolsDynamicImageGenerator'  => 'Use the expandable dynamic image generator',
    'SHOP_MODULE_bTRWToolsDontShowGuestUserInBackendList' => 'Dont show Guestuser in the backendlist',
    'SHOP_MODULE_bTRWToolsSetSameOriginHeader'            => 'Set SameOrigin headers',
    'HELP_SHOP_MODULE_bTRWToolsSetSameOriginHeader'       => 'The Same Origin Policy is a security concept that prohibits client-side scripting languages such as JavaScript, but also CSS, from accessing objects (for example graphics) that originate from or from another website Location does not match the origin. It represents an essential security element in all modern browsers and web applications for protection against attacks.',

    'SHOP_MODULE_bTRWToolsLoglevelOverride'                                       => 'Log Level übersteuern?',
    'HELP_SHOP_MODULE_bTRWToolsLoglevelOverride'                                  => 'Since PHP 8, warnings have also been logged in the error log by default. Depending on the web server configuration, log_errors is active in the default configuration. This results in the log file being “filled” accordingly. This can be used to override the behavior.',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault'                                => 'Log Level Options',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_default'                        => 'OXID Standard',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all'                            => 'All',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all_no_notice'                  => 'Everything except "notice"',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all_no_noticewarning'           => 'Everything except "notice" & "warning"',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_all_no_noticewarningdeprecated' => 'Everything except "notice" & "warning" & "deprecated"',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_just_errors'                    => 'Only Errors (ERROR)',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideDefault_flexible'                       => 'Own option',
    'SHOP_MODULE_sTRWToolsLoglevelOverrideFlex'                                   => 'Concrete own option',
];
