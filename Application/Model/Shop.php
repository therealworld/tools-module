<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Shop class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Shop
 */
class Shop extends Shop_parent
{
    use DataGetter;
}
