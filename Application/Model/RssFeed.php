<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use stdClass;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Tools  RSSFeed class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\RssFeed
 */
class RssFeed extends RssFeed_parent
{
    use DataGetter;

    /** getExceptionLogTitle get title for 'ExceptionLog' rss feed */
    public function getExceptionLogTitle(): string
    {
        return $this->_prepareFeedName(
            Registry::getLang()->translateString('TRWTOOLS_EXCEPTIONLOG')
        );
    }

    /**
     * getExceptionLogUrl get url for 'ExceptionLog' rss feed.
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function getExceptionLogUrl(): string
    {
        $sUrl = $this->_prepareUrl('cl=rss&amp;fnc=exceptionlog', $this->getExceptionLogTitle());
        $sUrl .= (Registry::getUtils()->seoIsActive() ? '?' : '&amp;');
        $sUrl .= 'token=' . $this->getExceptionLogToken();

        return $sUrl;
    }

    /**
     * create Token for Exception-Log-RSS-Feed.
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function getExceptionLogToken(): string
    {
        $oConfig = Registry::getConfig();
        if (!$sToken = $oConfig->getConfigParam('sTRWToolsExceptionLogRSSFeedToken')) {
            $sToken = Registry::getUtilsObject()->generateUid();
            ToolsConfig::saveConfigParam(
                'sTRWToolsExceptionLogRSSFeedToken',
                $sToken,
                'str',
                'trwtools'
            );
        }

        return $sToken;
    }

    /**
     * loadExceptionLog loads 'ExceptionLog' rss data.
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function loadExceptionLog(): void
    {
        $aLines = $this->_readBackwardFromTextFile(
            OX_LOG_FILE,
            (int) Registry::getConfig()->getConfigParam('iTRWToolsExceptionLogRSSFeedMaxEntries'),
            "\n["
        );

        $this->_loadData(
            null,
            $this->getExceptionLogTitle(),
            null,
            $this->_getExceptionLogEntries($aLines),
            $this->getExceptionLogUrl()
        );
    }

    /**
     * _getExceptionLogEntries create channel items from exception log.
     *
     * @param $aLines - array with Lines from log-File
     */
    protected function _getExceptionLogEntries(array $aLines = []): array
    {
        $aResult = [];
        $oStr = Str::getStr();

        foreach ($aLines as $sLine) {
            if ($aParsedLine = $this->_parseLogFileLine($sLine)) {
                $oItem = new stdClass();

                $oItem->title = $oStr->strip_tags($aParsedLine['title'] ?? $aParsedLine['desc']);
                $oItem->guid = md5($sLine);
                $oItem->description = $oStr->htmlspecialchars(trim($aParsedLine['desc']));
                $oItem->date = date('D, d M Y H:i:s O', $aParsedLine['time']);
                $oItem->link = Registry::getUtilsUrl()->prepareUrlForNoSession(Registry::getConfig()->getShopUrl());

                $aResult[] = $oItem;
            }
        }

        return $aResult;
    }

    /**
     * _readBackwardFromTextFile read a textfile backwards.
     *
     * @param $sFile      - filename
     * @param $iLineCount - number of lines
     * @param $sLineBreak - string for linebreak
     */
    protected function _readBackwardFromTextFile(string $sFile, int $iLineCount = 0, string $sLineBreak = "\n"): array
    {
        $aResult = [];

        if (file_exists($sFile)) {
            // 8kByte Buffer
            $iBuffer = 8 * 1024;

            $sLast = '';
            if ($oFileHandle = fopen($sFile, 'rb')) {
                fseek($oFileHandle, 0, SEEK_END);
                $iPos = ftell($oFileHandle);

                while ($iPos > 1) {
                    $iPos -= $iBuffer;
                    if ($iPos < 0) {
                        $iBuffer += $iPos;
                        $iPos = 0;
                    }
                    fseek($oFileHandle, $iPos);
                    $aTmp = explode($sLineBreak, fread($oFileHandle, $iBuffer) . $sLast);
                    if ($iPos > 0) {
                        $sLast = array_shift($aTmp);
                    }
                    $aResult += $aTmp;

                    // if we have doubled Log entries, we clean them ...
                    $aResult = array_unique($aResult);

                    if ($iLineCount && count($aResult) >= $iLineCount) {
                        break;
                    }
                }
                if ($iLineCount && count($aResult) >= $iLineCount) {
                    $aResult = array_slice($aResult, -$iLineCount);
                }
            }
        }

        return $aResult;
    }

    /**
     * _parseLogFileLine read a textfile backwards.
     *
     * @param $sLine - filename
     *
     * @return array (['desc' => '', 'time' => '',(optional: 'title' => '')])
     */
    protected function _parseLogFileLine(string $sLine): array
    {
        $aResult = [];

        $sLine = preg_replace("/\r|\n/", '', $sLine);

        // Lines written with \Monolog\Logger
        $mFoundMonolog = false;
        foreach (ToolsLog::getPossibleLogLevels() as $sLogLevel) {
            if (($mFoundMonolog = stripos($sLine, '.' . $sLogLevel . ':')) !== false) {
                break;
            }
        }

        if ($mFoundMonolog) {
            $sTimeTmp = substr($sLine, 0, $mFoundMonolog);
            $sDescTmp = substr($sLine, $mFoundMonolog);

            // Time
            $aTimeTmp = explode(']', $sTimeTmp);
            if ($sTime = trim($aTimeTmp[0], '[]')) {
                $aResult['time'] = strtotime($sTime);
            }

            // Description
            if (($mStriPos = strpos($sDescTmp, '[')) !== false) {
                $sTmpType = trim(substr($sDescTmp, 0, $mStriPos), '. ');
                $sTmpDesc = trim(substr($sDescTmp, $mStriPos));
                $sTmpDesc = str_replace(['["[object]', '\n"] []'], ['', ''], $sTmpDesc);
                $aResult['title'] = $sTmpType;

                $aTmpDesc = explode('\n', $sTmpDesc);
                $sDesc = '';
                foreach ($aTmpDesc as $sTmpDescItem) {
                    $sDesc .= sprintf(
                        '<li>%s</li>' . PHP_EOL,
                        $sTmpDescItem
                    );
                }

                $aResult['desc'] = ($sDesc ? sprintf(
                    '<ul>%s</ul>',
                    $sDesc
                ) : '');
            }
        } elseif (strpos($sLine, '[exception]') !== false) {
            // Lines written with \OxidEsales\Eshop\Core\Exception\ExceptionHandler
            $aTmp = explode('] [', $sLine);

            // Time
            if ($sTime = trim($aTmp[0], '[]')) {
                $aResult['time'] = strtotime($sTime);
            }

            // Description
            if (count($aTmp) > 1) {
                $sDesc = '';
                foreach ($aTmp as $sTmp) {
                    $sTmp = trim($sTmp, '[]');
                    if (($mStriPos = strpos($sTmp, ' ')) !== false) {
                        $sTmpType = trim(substr($sTmp, 0, $mStriPos));
                        $sTmpDesc = trim(substr($sTmp, $mStriPos));
                        $sDesc .= sprintf(
                            '<li><b>%s</b> - %s</li>' . PHP_EOL,
                            Str::getStr()->strtoupper($sTmpType),
                            $sTmpDesc
                        );
                        if ($sTmpType === 'type') {
                            $aResult['title'] = $sTmpDesc;
                        }
                    }
                }

                $aResult['desc'] = ($sDesc ? sprintf(
                    '<ul>%s</ul>',
                    $sDesc
                ) : '');
            }
        } elseif (strpos($sLine, '] ') !== false) {
            // Lines written with \OxidEsales\Eshop\bootstrap.php
            $aTmp = explode('] ', $sLine);

            // Time
            if ($sTime = trim($aTmp[0], '[]')) {
                $aResult['time'] = strtotime($sTime);
            }

            // Description
            if (isset($aTmp[1])) {
                $aResult['desc'] = $aTmp[1];
            }
        }

        return $aResult;
    }
}
