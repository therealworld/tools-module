<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Application\Model\Delivery;
use OxidEsales\Eshop\Application\Model\DeliveryList as OxidDeliveryList;
use OxidEsales\Eshop\Application\Model\DeliverySetList;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\TableViewNameGenerator;

/**
 * Tools DeliveryList class.
 *
 * @mixin OxidDeliveryList
 */
class DeliveryList extends DeliveryList_parent
{
    /**
     * Loads and return the delivery for a single product. Optimal for Exports.
     *
     * Process:
     *
     *  - loads delivery set list by calling this::GetDeliverySetList(...);
     *  - goes through delivery sets and loads its deliveries, checks if any
     *    delivery fits. By checking calculates and stores conditional
     *    amounts:
     *
     *       oDelivery->iItemCnt - items in basket that fits this delivery
     *       oDelivery->iProdCnt - products in basket that fits this delivery
     *       oDelivery->dPrice   - price of products that fits this delivery
     *
     *  - returns the last delivery.
     *
     * @param Article $oProduct    oxArticle object
     * @param string  $sDelCountry user country id
     *
     * @throws DatabaseConnectionException
     */
    public function getProductDelivery(Article $oProduct, string $sDelCountry = ''): ?Delivery
    {
        // ids of deliveries that does not fit for us to skip double check
        $aSkipDeliveries = [];
        $oUser = oxNew(User::class);
        $aDelSetList = Registry::get(DeliverySetList::class)->getDeliverySetList($oUser, $sDelCountry);

        // must choose right delivery set to use its delivery list
        foreach ($aDelSetList as $sDeliverySetId => $oDeliverySet) {
            // loading delivery list to check if some of them fits
            $aDeliveries = $this->_getListForProduct($oProduct, $sDelCountry, $sDeliverySetId);
            $blDelFound = false;

            foreach ($aDeliveries as $sDeliveryId => $oDelivery) {
                // skipping that was checked and didn't fit before
                if (in_array($sDeliveryId, $aSkipDeliveries, true)) {
                    continue;
                }
                $aSkipDeliveries[] = $sDeliveryId;

                // delivery fits conditions
                $aDeliveries[$sDeliveryId]->setChoosenDeliverySetId($sDeliverySetId);
                $this->_aDeliveries[$sDeliveryId] = $aDeliveries[$sDeliveryId];
                $blDelFound = true;

                // removing from unfitting list
                array_pop($aSkipDeliveries);

                // maybe checked "Stop processing after first match" ?
                if ($oDelivery->oxdelivery->getTRWStringData('oxfinalize')) {
                    break;
                }
            }

            // found delivery set and deliveries that fits
            if ($blDelFound) {
                return array_pop($this->_aDeliveries);
            }
        }

        return null;
    }

    /**
     * Load oxDeliveryList for product.
     *
     * @param Article $oProduct    given article
     * @param string  $sDelCountry target country id
     * @param string  $sDelSet     chosen delivery set
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseConnectionException
     */
    protected function _getListForProduct(
        Article $oProduct,
        string $sDelCountry = '',
        string $sDelSet = ''
    ): OxidDeliveryList {
        $oDb = DatabaseProvider::getDb();
        $dPrice = $oDb->quote($oProduct->getPrice()->getBruttoPrice());
        $dSize = $oDb->quote($oProduct->getSize());
        $dWeight = $oDb->quote($oProduct->getWeight());

        $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
        $oBaseObject = $this->getBaseObject();
        $sTable = $oBaseObject->getViewName();
        $sObj2DeliveryTable = $viewNameGenerator->getViewName('oxobject2delivery');
        $sDel2DeliverySetTable = $viewNameGenerator->getViewName('oxdel2delset');

        $sSelect = "select {$sTable}.* from {$sTable}";

        if ($sDelSet) {
            $sSelect .= " left join {$sDel2DeliverySetTable} on {$sDel2DeliverySetTable}.oxdelid = {$sTable}.oxid ";
        }

        $sSelect .= ' where ' . $this->getBaseObject()->getSqlActiveSnippet();
        $sSelect .= " and ({$sTable}.oxdeltype != 'a' || ( {$sTable}.oxparam <= 1 && {$sTable}.oxparamend >= 1))";

        if ($sDelSet) {
            $sSelect .= ' and oxdel2delset.oxdelsetid = ' . $oDb->quote($sDelSet) . ' ';
        }

        if ($dPrice) {
            $sSelect .= " and ({$sTable}.oxdeltype != 'p' ||
             ( {$sTable}.oxparam <= {$dPrice} && {$sTable}.oxparamend >= {$dPrice}))";
        }
        if ($dSize) {
            $sSelect .= " and ({$sTable}.oxdeltype != 's' ||
             ( {$sTable}.oxparam <= {$dSize} && {$sTable}.oxparamend >= {$dSize}))";
        }
        if ($dWeight) {
            $sSelect .= " and ({$sTable}.oxdeltype != 'w' ||
             ( {$sTable}.oxparam <= {$dWeight} && {$sTable}.oxparamend >= {$dWeight}))";
        }
        if ($sDelCountry) {
            $sSelect .= " and (
                {$sTable}.oxid in
                    ( select {$sObj2DeliveryTable}.oxdeliveryid
                        from {$sObj2DeliveryTable}, {$sTable}
                        where {$sObj2DeliveryTable}.oxdeliveryid = {$sTable}.oxid
                        and {$sObj2DeliveryTable}.oxtype = 'oxcountry'
                        and {$sObj2DeliveryTable}.oxobjectid = " . $oDb->quote($sDelCountry) . "
                    )
                or if ( exists (
                    select 1
                    from {$sObj2DeliveryTable}
                    where {$sObj2DeliveryTable}.oxobjectid = " . $oDb->quote($sDelCountry) . ') , 0, 1)
                )';
        }

        $this->selectString($sSelect);
        $this->rewind();

        return $this;
    }
}
