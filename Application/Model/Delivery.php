<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Core\Price;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Tools Delivery class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\Delivery
 */
class Delivery extends Delivery_parent
{
    use DataGetter;

    /** The ID of a possible choosen DeliverySet */
    protected ?string $_sChoosenDeliverySetId = null;

    /**
     * Returns price for delivery costs.
     *
     * @param float $dPrice article price
     * @param float $dVat   delivery vat
     */
    public function getProductDeliveryPrice(float $dPrice, float $dVat): float
    {
        $this->_blFreeShipping = false;
        $this->_dPrice = $dPrice;

        // loading oxPrice object for final price calculation
        $oPrice = oxNew(Price::class);
        $oPrice->setNettoMode($this->_blDelVatOnTop);
        $oPrice->setVat($dVat);
        $oPrice->add($this->_getCostSum());

        return $oPrice->getPrice();
    }

    /**
     * set a possible choosen DeliverySet.
     *
     * @param string $sId oxid of deliveryset
     */
    public function setChoosenDeliverySetId(string $sId): void
    {
        $this->_sChoosenDeliverySetId = $sId;
    }

    /** get a possible choosen DeliverySet */
    public function getChoosenDeliverySetId(): string
    {
        return $this->_sChoosenDeliverySetId;
    }
}
