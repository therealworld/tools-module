<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Application\Model\Content;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;

/**
 * Tools CategoryList class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\CategoryList
 */
class CategoryList extends CategoryList_parent
{
    /**
     * Loads all Category Oxids. It is optimal for Schedulerexports.
     *
     * @param int    $iStep                - Start position for SQL
     * @param int    $iLimit               - Limit position for SQL
     * @param bool   $bOnlyActive          - load only active Categories?
     * @param array  $aConditions          - more Conditions for SQL
     * @param bool   $bNoContentCategories - load no ContentCategories
     * @param string $sSortField           - Sortfield (e.g. oxsort)
     * @param bool   $bReverse             list order direction
     *
     * @throws DatabaseConnectionException
     */
    public function loadAllCategories(
        int $iStep = 0,
        int $iLimit = 0,
        bool $bOnlyActive = true,
        array $aConditions = [],
        bool $bNoContentCategories = false,
        string $sSortField = 'oxsort',
        bool $bReverse = false
    ): void {
        $oBaseObject = $this->getBaseObject();
        $sViewName = $oBaseObject->getViewName();

        $sCategoryFields = $oBaseObject->getSelectFields();
        $oDb = DatabaseProvider::getDb();

        $sLimit = '';
        $sVars = '';
        $sOnlyActive = '';
        $sContentCategories = '';

        if ($iLimit > 0) {
            $sLimit .= ' limit ';
            if ($iStep > 0) {
                $sLimit .= $iStep . ', ';
            }
            $sLimit .= $iLimit;
        }

        // more conditions
        if (count($aConditions)) {
            foreach ($aConditions as $aCondition) {
                // check if all necessary keys exists
                if (
                    count(
                        array_diff_key(
                            array_flip(
                                ['operator', 'condition', 'field', 'value', 'noquote']
                            ),
                            $aCondition
                        )
                    ) === 0
                ) {
                    $sVars .= ' ' . $aCondition['operator'];
                    $sVars .= " {$aCondition['field']} " . $aCondition['condition'] . ' '
                        . ($aCondition['noquote'] ? $aCondition['value'] : $oDb->quote($aCondition['value']));
                }
            }
        }

        // no Categories?
        if ($bNoContentCategories) {
            $oContent = oxNew(Content::class);
            $sContentTable = $oContent->getViewName();
            $sContentCategories .= " and {$sViewName}.oxid not in (
                select {$sContentTable}.oxcatid from {$sContentTable} where {$sContentTable}.oxtype = 2
            ) ";
        }

        // active?
        if ($bOnlyActive) {
            $sOnlyActive .= ' and ' . $oBaseObject->getSqlActiveSnippet();
        }

        $sSort = '';
        if ($sSortField) {
            $sSort = " order by {$sViewName}.{$sSortField} " . ($bReverse ? 'desc' : 'asc') . ' ';
        }

        $sSelect = "select {$sCategoryFields} from {$sViewName} " .
            'where 1 ' . $sVars . $sOnlyActive . $sContentCategories . $sSort . $sLimit;

        $this->selectString($sSelect);
    }
}
