<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\TableViewNameGenerator;

/**
 * Tools ArticleList class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\ArticleList
 */
class ArticleList extends ArticleList_parent
{
    /**
     * Loads all Article Oxids. It is optimal for Schedulerexports.
     *
     * @param int   $iStep         - Start position for SQL
     * @param int   $iLimit        - Limit position for SQL
     * @param bool  $bWithVars     - with variants
     * @param bool  $bWithMainVars - with Main variants
     * @param bool  $bOnlyActive   - load only active Article?
     * @param array $aCategories   - Articles from this categories
     * @param array $aManufactures - Articles from this manufacturers
     * @param array $aConditions   - more Conditions for SQL
     *
     * @throws DatabaseConnectionException
     */
    public function loadAllArticles(
        int $iStep = 0,
        int $iLimit = 0,
        bool $bWithVars = false,
        bool $bWithMainVars = false,
        bool $bOnlyActive = true,
        array $aCategories = [],
        array $aManufactures = [],
        array $aConditions = []
    ): void {
        $oBaseObject = $this->getBaseObject();
        $sViewName = $oBaseObject->getViewName();
        $sArticleFields = $oBaseObject->getSelectFields();
        $oDb = DatabaseProvider::getDb();

        $sVars = '';
        $sCategoriesJoin = '';
        $sCategories = '';
        $sOnlyActive = '';
        $sManufacturers = '';

        // check the Views
        if (($bWithVars && !$bWithMainVars) || !$bWithVars) {
            $sVars .= " and {$sViewName}.oxparentid " . ($bWithVars ? '!=' : '=') . " '' ";
        }

        // more conditions
        if (count($aConditions)) {
            foreach ($aConditions as $aCondition) {
                // check if all necessary keys exists
                if (
                    count(
                        array_diff_key(
                            array_flip(
                                ['operator', 'condition', 'field', 'value', 'noquote']
                            ),
                            $aCondition
                        )
                    ) === 0
                ) {
                    $sVars .= ' ' . $aCondition['operator'];
                    $sVars .= " {$aCondition['field']} " . $aCondition['condition'] . ' '
                        . ($aCondition['noquote'] ? $aCondition['value'] : $oDb->quote($aCondition['value']));
                }
            }
        }

        // check the Categories
        if (count($aCategories)) {
            $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
            $sO2CView = $viewNameGenerator->getViewName('oxobject2category');
            $sCategoriesJoin .= " left join {$sO2CView} on ({$sO2CView}.oxobjectid = {$sViewName}.oxid) ";
            $sCategories .= " and {$sO2CView}.oxcatnid in (" . implode(', ', $oDb->quoteArray($aCategories)) . ') ';
        }

        // check the Manufacturers
        if (count($aManufactures)) {
            $sManufacturers .= " and {$sViewName}.oxmanufacturerid in (" .
                implode(
                    ', ',
                    $oDb->quoteArray($aManufactures)
                )
                . ') ';
        }

        $sLimit = '';
        if ($iLimit > 0) {
            $sLimit .= ' limit ';
            if ($iStep > 0) {
                $sLimit .= $iStep . ', ';
            }
            $sLimit .= $iLimit;
        }

        // active?
        if ($bOnlyActive) {
            $sOnlyActive .= ' and ' . $oBaseObject->getSqlActiveSnippet();
        }

        $sSelect = "select {$sArticleFields} from {$sViewName} " .
            $sCategoriesJoin .
            'where 1 ' . $sVars . $sCategories . $sManufacturers . $sOnlyActive . $sLimit;

        $this->selectString($sSelect);
    }
}
