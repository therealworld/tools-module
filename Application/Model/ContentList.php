<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;

/**
 * Content list manager.
 * Collects list of content.
 */
class ContentList extends ContentList_parent
{
    /**
     * Loads all Content Oxids. It is optimal for Schedulerexports.
     *
     * @param int   $iStep       - Start position for SQL
     * @param int   $iLimit      - Limit position for SQL
     * @param bool  $bOnlyActive - load only active Contents?
     * @param array $aConditions - more Conditions for SQL
     *
     * @throws DatabaseConnectionException
     */
    public function loadAllContents(
        int $iStep = 0,
        int $iLimit = 0,
        bool $bOnlyActive = true,
        array $aConditions = []
    ): void {
        $oBaseObject = $this->getBaseObject();
        $sViewName = $oBaseObject->getViewName();
        $sContentFields = $oBaseObject->getSelectFields();
        $oDb = DatabaseProvider::getDb();

        $sLimit = '';
        $sVars = '';
        $sOnlyActive = '';

        if ($iLimit > 0) {
            $sLimit .= ' limit ';
            if ($iStep > 0) {
                $sLimit .= $iStep . ', ';
            }
            $sLimit .= $iLimit;
        }

        // more conditions
        if (count($aConditions)) {
            foreach ($aConditions as $aCondition) {
                // check if all necessary keys exists
                if (
                    count(
                        array_diff_key(
                            array_flip(
                                ['operator', 'condition', 'field', 'value', 'noquote']
                            ),
                            $aCondition
                        )
                    ) === 0
                ) {
                    $sVars .= ' ' . $aCondition['operator'];
                    $sVars .= " {$aCondition['field']} " . $aCondition['condition'] . ' '
                        . ($aCondition['noquote'] ? $aCondition['value'] : $oDb->quote($aCondition['value']));
                }
            }
        }

        // active?
        if ($bOnlyActive) {
            $sOnlyActive .= ' and ' . $oBaseObject->getSqlActiveSnippet();
        }

        $sSelect = "select {$sContentFields} from {$sViewName} " .
            'where 1 ' . $sVars . $sOnlyActive . $sLimit;

        $this->selectString($sSelect);
    }
}
