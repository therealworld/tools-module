<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Model;

use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Model\ListModel;

/**
 * Tools OrderList class.
 */
class OrderList extends ListModel
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct(Order::class);
    }

    /**
     * Loads all Order Oxids. It is optimal for Schedulerexports.
     *
     * @param int   $iStep       - Start position for SQL
     * @param int   $iLimit      - Limit position for SQL
     * @param array $aConditions - more Conditions for SQL
     *                           [
     *                           [
     *                           'name'      => 'oxtransstatus',
     *                           'value'     => 'OK',
     *                           'condition' => '<>', // optional
     *                           'noquote'   => false, // optional
     *                           ]
     *                           ], ...
     *
     * @throws DatabaseConnectionException
     */
    public function loadAllOrders(int $iStep = 0, int $iLimit = 0, array $aConditions = []): void
    {
        $oBaseObject = $this->getBaseObject();
        $sViewName = $oBaseObject->getViewName();
        $sArticleFields = $oBaseObject->getSelectFields();
        $oDb = DatabaseProvider::getDb();

        $sLimit = '';
        $sVars = '';

        if ($iLimit > 0) {
            $sLimit .= ' limit ';
            if ($iStep > 0) {
                $sLimit .= $iStep . ', ';
            }
            $sLimit .= $iLimit;
        }

        // more conditions
        if (count($aConditions)) {
            foreach ($aConditions as $aCondition) {
                // check if all necessary keys exists
                if (
                    count(
                        array_diff_key(
                            array_flip(
                                ['operator', 'condition', 'field', 'value', 'noquote']
                            ),
                            $aCondition
                        )
                    ) === 0
                ) {
                    $sVars .= ' ' . $aCondition['operator'];
                    $sVars .= " {$aCondition['field']} " . $aCondition['condition'] . ' '
                        . ($aCondition['noquote'] ? $aCondition['value'] : $oDb->quote($aCondition['value']));
                }
            }
        }

        $sSelect = "select {$sArticleFields} from {$sViewName}
            where 1 " . $sVars . "
            order by {$sViewName}.oxordernr asc " . $sLimit;

        $this->selectString($sSelect);
    }
}
