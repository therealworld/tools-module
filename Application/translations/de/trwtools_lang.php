<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TRWTOOLSPLUGIN_PATH_NOT_DEFINED'                                                            => 'Pfad nicht definiert',
    'TRWTOOLSPLUGIN_PATH_NOT_FOUND'                                                              => 'Pfad nicht gefunden: %s',
    'TRWTOOLSPLUGIN_PATH_NOT_READABLE'                                                           => 'Pfad hat keine Leserechte: %s',
    'TRWTOOLSPLUGIN_PFAD_NOT_WRITABLE'                                                           => 'Pfad hat keine Schreibrechte: %s',
    'TRWTOOLSPLUGIN_FILE_NOT_FOUND'                                                              => 'Datei nicht gefunden: %s',
    'TRWTOOLSPLUGIN_CANT_CREATE_PATH'                                                            => 'Konnte Pfad nicht anlegen: %s',
    'TRWTOOLSPLUGIN_COULD_NOT_OPEN_FILE'                                                         => 'Kann Datei nicht öffnen: %s',
    'TRWTOOLSPLUGIN_BACKUP_CREATED'                                                              => 'Backup erstellt: %s -> %s',
    'TRWTOOLSPLUGIN_FILE_COPIED'                                                                 => 'Datei kopiert: %s -> %s',
    'TRWTOOLSPLUGIN_FILE_MOVED'                                                                  => 'Datei verschoben: %s -> %s',
    'TRWTOOLSPLUGIN_FAILED_TO_MOVE_OR_RENAME_THE_FILE'                                           => 'Fehler beim verschieben bzw. umbenennen der Datei: %s -> %s',
    'TRWTOOLSPLUGIN_EXTRACT'                                                                     => 'entpacke: %s',
    'TRWTOOLSPLUGIN_FOUND_FOLLOW_FILE_S'                                                         => 'Folgende Datei(en) gefunden: %s',
    'TRWTOOLSPLUGIN_API_FILEGETCONTENTS_HAS_PROBLEMS_WITH_SSL_IN_SECOND_TRY_WITHOUT_PEER_VERIFY' => 'API FileGetContents hat Probleme mit SSL in %s (%s), zweiter Versuch ohne peer verify',
    'TRWTOOLSPLUGIN_API_FILEGETCONTENTS_HAS_PROBLEMS_WITH_SSL_AND_PEER_VERIFY_IN'                => 'API FileGetContents hat Probleme mit SSL und peer verify in %s  (%s)',

    'TRWTOOLS_EXCEPTIONLOG' => 'Exception-Log RSS-Feed',
];
