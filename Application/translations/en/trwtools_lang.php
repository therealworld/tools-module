<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TRWTOOLSPLUGIN_PATH_NOT_DEFINED'                                                            => 'Path not defined',
    'TRWTOOLSPLUGIN_PATH_NOT_FOUND'                                                              => 'Path not found: %s',
    'TRWTOOLSPLUGIN_PATH_NOT_READABLE'                                                           => 'Path not readable: %s',
    'TRWTOOLSPLUGIN_PFAD_NOT_WRITABLE'                                                           => 'Pfad not writable: %s',
    'TRWTOOLSPLUGIN_FILE_NOT_FOUND'                                                              => 'File not found: %s',
    'TRWTOOLSPLUGIN_CANT_CREATE_PATH'                                                            => 'Cant create path: %s',
    'TRWTOOLSPLUGIN_COULD_NOT_OPEN_FILE'                                                         => 'Could not open file: %s',
    'TRWTOOLSPLUGIN_BACKUP_CREATED'                                                              => 'Backup created: %s -> %s',
    'TRWTOOLSPLUGIN_FILE_COPIED'                                                                 => 'File copied: %s -> %s',
    'TRWTOOLSPLUGIN_FILE_MOVED'                                                                  => 'File moved: %s -> %s',
    'TRWTOOLSPLUGIN_FAILED_TO_MOVE_OR_RENAME_THE_FILE'                                           => 'Failed to move or rename the file: %s -> %s',
    'TRWTOOLSPLUGIN_EXTRACT'                                                                     => 'extract: %s',
    'TRWTOOLSPLUGIN_FOUND_FOLLOW_FILE_S'                                                         => 'found follow file(s): %s',
    'TRWTOOLSPLUGIN_API_FILEGETCONTENTS_HAS_PROBLEMS_WITH_SSL_IN_SECOND_TRY_WITHOUT_PEER_VERIFY' => 'API FileGetContents has Problems with SSL in %s (%s), second try without peer verify',
    'TRWTOOLSPLUGIN_API_FILEGETCONTENTS_HAS_PROBLEMS_WITH_SSL_AND_PEER_VERIFY_IN'                => 'API FileGetContents has Problems with SSL and peer verify in %s (%s)',

    'TRWTOOLS_EXCEPTIONLOG' => 'Exception-Log RSS-Feed',
];
