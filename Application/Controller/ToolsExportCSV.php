<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsString;

/**
 * Tools Export CSV class.
 */
class ToolsExportCSV
{
    /** String of a CSV Delimiter */
    protected string $_sDelimiter = ';';

    /** String of a CSV Enclosure */
    protected string $_sEnclosure = '"';

    /** The Filename of the exportfile */
    protected string $_sFileName = 'export.csv';

    /** The path */
    protected string $_sExportPath = 'export/';

    /** The Name of the Data List Object */
    protected ?string $_sDataListObj = null;

    /** Export in UTF-8? */
    protected bool $_bExportUTF8 = true;

    /** Export with Header? */
    protected bool $_bWithHeader = true;

    /** The Name of the Data List Method */
    protected ?string $_sDataListMethod = null;

    /** The Export Schema */
    protected array $_aExportSchema = [];

    /** The Limiter Schema */
    protected array $_aLimiterSchema = [];

    /** The Replace Schema */
    protected array $_aReplaceSchema = [];

    /** The Set After Export */
    protected array $_aSetAfterExport = [];

    /** all the fields that not convert html-codes like <>" */
    protected array $_aFieldsWithHtmlProtection = [];

    /** all the fields that should have no HTML entities */
    protected array $_aEntityDecodes = [];

    /**
     * set a Class Option.
     */
    public function setOptions(array $aOptions = []): void
    {
        foreach ($aOptions as $sOptionName => $sOptionValue) {
            $this->{$sOptionName} = $sOptionValue;
        }
    }

    /**
     * write the export Data.
     *
     * @param int  $iStep               - Start position for SQL
     * @param int  $iLimit              - Limit position for SQL
     * @param bool $bForceCreateNewFile - force to create a new file
     *
     * @return null|int - the step, or false, if not writeable
     *
     * @throws DatabaseConnectionException
     */
    public function writeExportData(int $iStep = 0, int $iLimit = 0, bool $bForceCreateNewFile = false): ?int
    {
        $bResult = false;

        $oDataList = oxNew($this->_sDataListObj);
        $sDataListMethod = $this->_sDataListMethod;

        call_user_func([
            $oDataList,
            $sDataListMethod,
        ], $iStep, $iLimit, array_keys($this->_aExportSchema));

        if ($oDataList->count()) {
            // In the first step we delete the existing exportfile
            if ($iStep === 0 || $bForceCreateNewFile) {
                ToolsFile::deleteFile($this->_getExportFile());
            }

            if ($oHandle = fopen($this->_getExportFile(), 'ab')) {
                $bResult = true;

                // In the first step we write the Header
                if ($iStep === 0) {
                    // set a UTF-8-BOM
                    if ($this->_bExportUTF8) {
                        fwrite($oHandle, "\xEF\xBB\xBF");
                    }

                    if ($this->_bWithHeader) {
                        fputcsv(
                            $oHandle,
                            $this->_aExportSchema,
                            $this->_sDelimiter,
                            $this->_sEnclosure
                        );
                    }
                }

                foreach ($oDataList as $oDataListItem) {
                    $aTmp = [];
                    foreach ($this->_aExportSchema as $sKey => $sValue) {
                        $sString = $oDataListItem->getTRWStringData($sKey);
                        $sLowKey = Str::getStr()->strtolower($sKey);
                        if (isset($this->_aFieldsWithHtmlProtection[$sLowKey])) {
                            $sString = htmlspecialchars_decode($sString);
                        } elseif ($this->_bExportUTF8) {
                            $sString = ToolsString::encodeUtf8($sString);
                        } else {
                            $sString = ToolsString::decodeUtf8($sString);
                        }
                        if (isset($this->_aEntityDecodes[$sLowKey])) {
                            $sString = html_entity_decode(
                                $sString,
                                ENT_XHTML,
                                $this->_bExportUTF8 ? 'UTF-8' : 'ISO-8859-15'
                            );
                        }

                        if (isset($this->_aReplaceSchema[$sKey])) {
                            foreach ($this->_aReplaceSchema[$sKey] as $sSearch => $sReplace) {
                                $sString = str_replace($sSearch, $sReplace, $sString);
                            }
                        }

                        if (isset($this->_aLimiterSchema[$sKey])) {
                            $sString = substr($sString, 0, $this->_aLimiterSchema[$sKey]);
                        }

                        $aTmp[] = $sString;
                    }
                    $iStep++;
                    fputcsv(
                        $oHandle,
                        $aTmp,
                        $this->_sDelimiter,
                        $this->_sEnclosure
                    );

                    $this->_setAfterExport($oDataListItem);
                }
                fclose($oHandle);
            }
        } else {
            $iStep = 0;
            $bResult = true;
        }

        return $bResult ? $iStep : null;
    }

    /** Datamanipulation after Export.
     *
     * @throws DatabaseConnectionException
     */
    protected function _setAfterExport(object $oDataListItem): void
    {
        if ($this->_aSetAfterExport) {
            foreach ($this->_aSetAfterExport as $sSetAfterExport) {
                [$sTable, $sCheckField, $sSetField, $sSetValue, $bCheckShopId] = explode('|', $sSetAfterExport);
                $sCheckValue = $oDataListItem->getTRWStringData($sCheckField);
                if ($sTable && $sCheckField && $sCheckValue && $sSetField && $sSetValue) {
                    $aParams = [
                        $sSetField => $sSetValue,
                    ];

                    $aOperator = [
                        [
                            'operator'  => 'and',
                            'condition' => '=',
                            'field'     => $sCheckField,
                            'value'     => $sCheckValue,
                            'noquote'   => false,
                        ],
                    ];

                    $sSql = ToolsDB::createSingleTableExecuteSql(
                        $sTable,
                        $aParams,
                        'update',
                        $aOperator,
                        (bool)$bCheckShopId
                    );

                    ToolsDB::execute($sSql);
                }
            }
        }
    }

    /**
     * get the name of the Export File.
     *
     * @param bool $bWithPath - filename with path?
     */
    protected function _getExportFile(bool $bWithPath = true): string
    {
        $sResult = '';
        if ($this->_sFileName) {
            if ($bWithPath) {
                $sResult .= Registry::getConfig()->getConfigParam('sShopDir') . $this->_sExportPath;
            }
            $sResult .= $this->_sFileName;
        }

        return $sResult;
    }
}
