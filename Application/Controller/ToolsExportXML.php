<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsFile;
use TheRealWorld\ToolsPlugin\Core\ToolsXML;

/**
 * Tools Export XML class.
 */
class ToolsExportXML
{
    /** The Filename of the exportfile */
    protected string $_sFileName = 'export.xml';

    /** The complete Exportfile with name and path */
    protected string $_sExportPath = 'export/';

    /** The Main XML Node */
    protected string $_sXMLStartNode = 'items';

    /** The Main XML Node Attributes */
    protected array $_aXMLStartNodeAttributes = [];

    /** set Class Options */
    public function setOptions(array $aOptions = []): void
    {
        foreach ($aOptions as $sOptionName => $sOptionValue) {
            $this->{$sOptionName} = $sOptionValue;
        }
    }

    /** init actual XML Structure */
    public function initXML(bool $bWithDelete = true): void
    {
        if ($bWithDelete) {
            ToolsFile::deleteFile($this->_getExportFile());
        }
        ToolsXML::setXMLFileName($this->_getExportFile(false), $this->_sExportPath);
        ToolsXML::setStartNode($this->_sXMLStartNode);
        ToolsXML::setStartNodeAttributes($this->_aXMLStartNodeAttributes);
    }

    /**
     * write the export Data.
     *
     * @param array $aData  - Array with Data to write
     * @param bool  $bFirst - is first run
     */
    public function writeExportData(array $aData, bool $bFirst): bool
    {
        return ToolsXML::setItemsToXML($aData, $bFirst, false);
    }

    /**
     * close actual XML Structure.
     */
    public function closeXML(): bool
    {
        return ToolsXML::setItemsToXML([], false);
    }

    /**
     * get the name of the Export File.
     *
     * @param bool $bWithPath - filename with path?
     */
    protected function _getExportFile(bool $bWithPath = true): string
    {
        $sResult = '';
        if ($this->_sFileName) {
            if ($bWithPath) {
                $sResult .= Registry::getConfig()->getConfigParam('sShopDir') . $this->_sExportPath;
            }
            $sResult .= $this->_sFileName;
        }

        return $sResult;
    }
}
