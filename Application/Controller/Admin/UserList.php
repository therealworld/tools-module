<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;

/**
 * User List class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\UserList
 */
class UserList extends UserList_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function _prepareWhereQuery($aWhere, $sQueryFull)
    {
        $sQ = parent::_prepareWhereQuery($aWhere, $sQueryFull);

        if (Registry::getConfig()->getConfigParam('bTRWToolsDontShowGuestUserInBackendList')) {
            $sQ .= " and 
            if (oxuser.oxrights = 'user' and oxuser.oxpassword != '', 1,
                if(oxuser.oxrights != 'user', 1, 0)
            ) ";
        }

        return $sQ;
    }
}
