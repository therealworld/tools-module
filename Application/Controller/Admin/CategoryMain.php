<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;

/**
 * Category Main class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\CategoryMain
 */
class CategoryMain extends CategoryMain_parent
{
    /**
     * export the categorytree to csv.
     */
    public function exportCategoryTree(): void
    {
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=categorytree.csv');

        // UTF-8 BOM
        echo "\xEF\xBB\xBF";

        $oCategoryTree = $this->oCatTree;
        foreach ($oCategoryTree as $oCategoryTreeElement) {
            $sTitle = $oCategoryTreeElement->getTRWRawStringData('oxtitle');
            $sOxId = $oCategoryTreeElement->getId();
            if ($sTitle !== '--' && $sOxId) {
                echo sprintf(
                    '"%s";"%s"' . "\n",
                    $sTitle,
                    $sOxId
                );
            }
        }
        Registry::getUtils()->showMessageAndExit('');
    }
}
