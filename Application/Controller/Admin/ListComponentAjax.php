<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller\Admin;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

/**
 * AJAX call processor class.
 */
class ListComponentAjax extends ListComponentAjax_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    protected function _getFilter()
    {
        $oConfig = Registry::getConfig();

        $sQ = parent::_getFilter();

        if ($oConfig->getConfigParam('bTRWToolsSearchContaining')) {
            $sQ = '';
            $aFilter = $oConfig->getRequestParameter('aFilter');
            if (is_array($aFilter) && count($aFilter)) {
                $aCols = $this->_getVisibleColNames();
                $oDb = DatabaseProvider::getDb();
                $oStr = Str::getStr();

                foreach ($aFilter as $sCol => $sValue) {
                    // skipping empty filters
                    if ($sValue === '') {
                        continue;
                    }

                    $iCol = (int) str_replace('_', '', $sCol);
                    if (isset($aCols[$iCol])) {
                        if ($sQ) {
                            $sQ .= ' and ';
                        }

                        // escaping special characters
                        $sValue = str_replace(['%', '_'], ['\%', '\_'], $sValue);

                        // possibility to search in the middle ..
                        $sValue = $oStr->preg_replace('/^\*/', '%', $sValue);

                        $sQ .= $this->_getViewName($aCols[$iCol][1]) . '.' . $aCols[$iCol][0];
                        $sQ .= ' like ' . $oDb->Quote('%' . $sValue . '%') . ' ';
                    }
                }
            }
        }

        return $sQ;
    }
}
