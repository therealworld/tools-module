<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\RssFeed;
use OxidEsales\Eshop\Core\Registry;

/**
 * ModuleConfiguration class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration
 */
class ModuleConfiguration extends ModuleConfiguration_parent
{
    /** Name of own root img creator file */
    protected string $_sToolsDynImgGen = 'trwtoolsgetimg.php';

    /** Name of oxid root img creator file */
    protected string $_sOxidDynImgGen = 'getimg.php';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function saveConfVars()
    {
        parent::saveConfVars();

        $oRequest = Registry::getRequest();
        $this->resetContentCache();
        $sModuleId = $this->_getModuleForConfigVars();

        if ($sModuleId === 'module:trwtools') {
            foreach ($this->_aConfParams as $sParam) {
                $aConfVars = $oRequest->getRequestParameter($sParam);
                if (is_array($aConfVars)) {
                    foreach ($aConfVars as $sName => $sValue) {
                        // check database
                        if ($sName === 'bTRWToolsUseToolsDynamicImageGenerator') {
                            $this->activateToolsDynamicImageGenerator($sValue === 'true');
                        }
                    }
                }
            }
        }
    }

    /**
     * Template getter get ExceptionLogRSSLink.
     */
    public function getExceptionLogRSSLink(): array
    {
        $oRss = oxNew(RssFeed::class);

        return [
            'title' => $oRss->getExceptionLogTitle(),
            'link'  => $oRss->getExceptionLogUrl(),
            'token' => $oRss->getExceptionLogToken(),
        ];
    }

    /**
     * activate the own dynamic ImageGenerator.
     *
     * @param mixed $bActive
     */
    protected function activateToolsDynamicImageGenerator($bActive = true): void
    {
        $sFileHtaccess = Registry::getConfig()->getConfigParam('sShopDir') . DIRECTORY_SEPARATOR . '.htaccess';

        $sContentHtaccess = file_get_contents($sFileHtaccess);
        $sContentSearch = '$ ' . $this->_sOxidDynImgGen . ' [NC]';
        $sEscapedDirectorySeperator = '\/';

        $sContentReplace = '$ modules'
            . $sEscapedDirectorySeperator . 'trw'
            . $sEscapedDirectorySeperator . 'trwtools'
            . $sEscapedDirectorySeperator . 'Root'
            . $sEscapedDirectorySeperator . $this->_sToolsDynImgGen . ' [NC]';

        if (!$bActive) {
            [$sContentSearch, $sContentReplace] = [$sContentReplace, $sContentSearch];
        }

        if (strpos($sContentHtaccess, $sContentSearch) !== false) {
            $sContentHtaccess = str_replace(
                $sContentSearch,
                $sContentReplace,
                $sContentHtaccess
            );
        }
        file_put_contents($sFileHtaccess, $sContentHtaccess);
    }
}
