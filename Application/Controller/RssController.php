<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Application\Controller;

use OxidEsales\Eshop\Core\Registry;

/**
 * Shop RSS page.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\RssController
 */
class RssController extends RssController_parent
{
    /**
     * loads exceptionlog entries to rss.
     */
    public function exceptionlog(): void
    {
        $oConfig = Registry::getConfig();
        if (
            $oConfig->getConfigParam('bTRWToolsExceptionLogRSSFeedActive')
            && $oConfig->getConfigParam('sTRWToolsExceptionLogRSSFeedToken')
            && $oConfig->getConfigParam('sTRWToolsExceptionLogRSSFeedToken') === $oConfig->getRequestParameter('token')
        ) {
            $this->_getRssFeed()->loadExceptionLog();
        } else {
            error_404_handler();
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getCacheLifeTime()
    {
        return (Registry::getRequest()->getRequestParameter('fnc') === 'exceptionlog') ? 0 : parent::getCacheLifeTime();
    }
}
