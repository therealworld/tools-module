<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

namespace
{
    use TheRealWorld\ToolsPlugin\Core\ToolsDynamicImageGenerator;

    // including generator class
    function getGeneratorInstanceName()
    {
        return ToolsDynamicImageGenerator::class;
    }

    // including generator class
    require_once __DIR__ .
        DIRECTORY_SEPARATOR . ".." .
        DIRECTORY_SEPARATOR . ".." .
        DIRECTORY_SEPARATOR . ".." .
        DIRECTORY_SEPARATOR . ".." .
        DIRECTORY_SEPARATOR . "bootstrap.php";

    // rendering requested image
    ToolsDynamicImageGenerator::getInstance()->outputImage();
}
