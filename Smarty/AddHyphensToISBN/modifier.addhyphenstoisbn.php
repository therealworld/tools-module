<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

use Isbn\Isbn;

/**
 * Smarty addhyphenstoisbn modifier plugin.
 *
 * Type:     modifier<br>
 * Name:     addhyphenstoisbn<br>
 * Date:     09.09.2019
 * Purpose:  Add Hyphens to an valid ISBN (special EAN for Books), if the ISBN isnt valid, no Hyphens would be added
 * Input:    string to add hyphens to
 * Example:  [{assign var="variable" value="ISBN"|addhyphenstoisbn}]
 *           ISBN = '9783423117371', $variable = '978-3-423-11737-1'
 *
 * @param string $sISBN string to add colon to
 *
 * @throws \Isbn\Exception
 */
function smarty_modifier_addhyphenstoisbn(string $sISBN): string
{
    $oISBN = new Isbn();

    if ($oISBN->check->is10($sISBN) || $oISBN->check->is13($sISBN)) {
        $sISBN = $oISBN->hyphens->addHyphens($sISBN);
    }

    return $sISBN;
}
