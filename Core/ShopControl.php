<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsModule\Traits\ErrorReportingLevel;

class ShopControl extends ShopControl_parent
{
    use ErrorReportingLevel;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _handleSystemException($exception) // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        $validator = new ExceptionValidator();
        if ($validator->isInvalid($exception)) {
            // do not log like in the parent method, just redirect
            Registry::getUtils()->redirect(
                Registry::getConfig()->getShopHomeUrl() . 'cl=start'
            );

            return;
        }

        parent::_handleSystemException($exception);
    }
}
