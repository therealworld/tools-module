<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class ToolsEvents
{
    /**
     * OXID-Core.
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    public static function onActivate()
    {
        // create intern monolog table
        $sCreateSql = "create table if not exists `trwtoolsmonolog` (
            `OXID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
            `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)',
            `OXCHANNEL` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Monolog: channel',
            `OXLEVEL` int(11) DEFAULT NULL COMMENT 'Monolog: level',
            `OXMESSAGE` longtext collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Monolog: message',
            `OXDATETIME` decimal(20,6) unsigned NULL COMMENT 'Monolog: datetime',
            primary key (`OXID`),
            index(OXCHANNEL) using hash, index(OXLEVEL) using hash, index(OXDATETIME) using btree
            ) engine=InnoDB default CHARSET=utf8 comment 'the-real-world: Monolog Logs'";

        ToolsDB::execute($sCreateSql);

        // additional Module-Update v1.9
        if (!ToolsDB::tableColumnExists('trwtoolsmonolog', 'OXSHOPID')) {
            $sAddSql = "ALTER TABLE `trwtoolsmonolog` ADD `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)'";
            ToolsDB::execute($sAddSql);
        }

        // additional Module-Update v2.3
        if (stripos(ToolsDB::getFieldInformation('trwtoolsmonolog', 'OXDATETIME'), 'int') === 0) {
            $sAddSql = "ALTER TABLE `trwtoolsmonolog` CHANGE `OXDATETIME` `OXDATETIME` decimal(20,6) unsigned NULL COMMENT 'Monolog: datetime'";
            ToolsDB::execute($sAddSql);
        }

        // SQLs for create the mysql cologne phonetic functions
        $sDeleteSql = 'drop function if exists `fnc_strip_non_alpha`';
        ToolsDB::execute($sDeleteSql);
        $sDeleteSql = 'drop function if exists `fnc_soundex_de`';
        ToolsDB::execute($sDeleteSql);
        $sDeleteSql = 'drop function if exists `fnc_cologne_match`';
        ToolsDB::execute($sDeleteSql);

        $sCreateSql = "create function fnc_strip_non_alpha (string varchar(255)) returns varchar(255)
            deterministic
            begin
                declare res varchar(255) default '';
                set string = lower(string);
                while char_length(string) > 0 do
                if string regexp '^[a-zäöüß]' then
                set res = concat(res, substr(string, 1, 1));
            end if;
            set string = substr(string, 2);
            end while;
            return res;
            end;";
        ToolsDB::execute($sCreateSql);

        $sCreateSql = "create function `fnc_soundex_de` (string varchar(255)) returns varchar(255)
            deterministic
            begin
                declare res varchar(255) default '';
                declare tmp varchar(255) default '';
                declare i tinyint unsigned default 1;
                set string = fnc_strip_non_alpha(string);
                while char_length(string) > 0 do
                if string regexp '^[0-9]' then
                set res = concat(res, substr(string, 1, 1));
                set string = substr(string, 2);
                elseif string regexp '^[aeijouyäöüÄÖÜ]' then
                set res = concat(res, '0');
                set string = substr(string, 2);
                elseif string regexp '^ph' then
                set res = concat(res, '3');
                set string = substr(string, 3);
                elseif string regexp '^[bp]' then
                set res = concat(res, '1');
                set string = substr(string, 2);
                elseif string regexp '^[dt][csz]' then
                set res = concat(res, '8');
                set string = substr(string, 3);
                elseif string regexp '^[dt]' then
                set res = concat(res, '2');
                set string = substr(string, 2);
                elseif string regexp '^[fvw]' then
                set res = concat(res, '3');
                set string = substr(string, 2);
                elseif string regexp '^[gkq]' then
                set res = concat(res, '4');
                set string = substr(string, 2);
                elseif string regexp '^c[ahkloqrux]' then
                set res = concat(res, '4');
                set string = substr(string, 2);
                elseif string regexp '^[^sz]c[ahkloqrux]' then
                set string = concat(substr(string, 1, 1), '4', substr(string, 3));
                elseif string regexp '^[^ckq]x+' then
                set tmp = substr(string, 1, 1);
                set string = substr(string, 3);
                while substr(string, 1, 1) = 'x' do
                set string = substr(string, 2);
            end while;
            set string = concat(tmp, 48, string);
            elseif string regexp '^l' then
            set res = concat(res, '5');
            set string = substr(string, 2);
            elseif string regexp '^[mn]' then
            set res = concat(res, '6');
            set string = substr(string, 2);
            elseif string regexp '^r' then
            set res = concat(res, '7');
            set string = substr(string, 2);
            elseif string regexp '^[sz]c' then
            set string = concat(substr(string, 1, 1), '8', substr(string, 3));
            elseif string regexp '^[szß]' then
            set res = concat(res, '8');
            set string = substr(string, 2);
            elseif string regexp '^c[^ahkloqrux]?' then
            set res = concat(res, '8');
            set string = substr(string, 2);
            elseif string regexp '^[ckq]x' then
            set string = concat(substr(string, 1, 1), '8', substr(string, 3));
            else
            set string = substr(string, 2);
            end if;
            end while;
            set res = replace(res, '070', '@');
            set res = replace(res, '07', '0');
            set res = replace(res, '@', '070'); # improved handling of silent r after vowels
            set res = replace(res, '3', '8'); # for phone calls, where F sounds like S
            set tmp = '@';
            while i <= char_length(res) do
            if substr(res, i, 1) = tmp then
            set res = concat(substr(res, 1, i - 1), substr(res, i + 1));
            else
            set tmp = substr(res, i, 1);
            set i = i + 1;
            end if;

            end while;
            set res = concat(substr(res, 1, 1), replace(substr(res, 2), '0', ''));
            return res;
            end;";
        ToolsDB::execute($sCreateSql);

        $sCreateSql = "create function fnc_cologne_match (needle varchar(128), haystack text, splitChar varchar(1)) returns tinyint
            deterministic
            begin
                declare spacePos int;
                declare searchLen int default length(haystack);
                declare curWord varchar(128) default '';
                declare tempStr text default haystack;
                declare tmp text default '';
                declare soundx1 varchar(64) default fnc_soundex_de(needle);
                declare soundx2 varchar(64) default '';

                set spacePos = locate(splitChar, tempStr);

                while searchLen > 0 do
                if spacePos = 0 then
                set tmp = tempStr;
                select fnc_soundex_de(tmp) into soundx2;
                if soundx1 = soundx2 then
                return 1;
                else
                return 0;
            end if;
            end if;

            if spacePos != 0 then
            set tmp = substr(tempStr, 1, spacePos-1);
            set soundx2 = fnc_soundex_de(tmp);
            if soundx1 = soundx2 then
            return 1;
            end if;
            set tempStr = substr(tempStr, spacePos+1);
            set searchLen = length(tempStr);
            end if;

            set spacePos = locate(splitChar, tempStr);

            end while;

            return 0;

            end";
        ToolsDB::execute($sCreateSql);

        return true;
    }

    /**
     * OXID-Core.
     */
    public static function onDeactivate()
    {
        // for security set return true
        return true;
        $sDeleteSql = 'drop table if exists `trwlog`';
        ToolsDB::execute($sDeleteSql);
        $sDeleteSql = 'drop function if exists `fnc_strip_non_alpha`';
        ToolsDB::execute($sDeleteSql);
        $sDeleteSql = 'drop function if exists `fnc_soundex_de`';
        ToolsDB::execute($sDeleteSql);
        $sDeleteSql = 'drop function if exists `fnc_cologne_match`';
        ToolsDB::execute($sDeleteSql);

        return true;
    }
}
