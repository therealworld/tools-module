<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use Monolog\Logger;
use OxidEsales\Eshop\Core\Str;
use TheRealWorld\ToolsModule\Vendor\Monolog\OxidDBHandler;

class ToolsMonologDBLogger
{
    /** DBLogger */
    protected static array $_aDBLogger = [];

    /** get Logger */
    public static function getDBLogger(string $sLogger = '', string $sLogLevel = ''): Logger
    {
        $sLogger = $sLogger ?: 'OXID Logger';

        if (!array_key_exists($sLogger, self::$_aDBLogger)) {
            $sLogLevel = Str::getStr()->strtoupper($sLogLevel);
            $iLogLevel = defined('Logger::' . $sLogLevel) ? constant('Logger::' . $sLogLevel) : Logger::DEBUG;

            self::$_aDBLogger[$sLogger] = new Logger($sLogger);
            self::$_aDBLogger[$sLogger]->pushHandler(new OxidDBHandler(
                $iLogLevel
            ));
        }

        return self::$_aDBLogger[$sLogger];
    }
}
