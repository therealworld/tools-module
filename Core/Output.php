<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use OxidEsales\Eshop\Core\Registry;

/**
 * class for output processing.
 */
class Output extends Output_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function sendHeaders()
    {
        if (Registry::getConfig()->getConfigParam('bTRWToolsSetSameOriginHeader')) {
            Registry::getUtils()->setHeader('X-Frame-Options: SAMEORIGIN');
        }
        parent::sendHeaders();
    }
}
