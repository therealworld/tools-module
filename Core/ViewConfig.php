<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use TheRealWorld\ToolsPlugin\Traits\ViewConfigTrait;

/**
 * View config data access class. Keeps most
 * of getters needed for formatting various urls,
 * config parameters, session information etc.
 */
class ViewConfig extends ViewConfig_parent
{
    use ViewConfigTrait;
}
