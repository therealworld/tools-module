<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\TableViewNameGenerator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsLog;

class ToolsDBLog
{
    /** Log-Page */
    protected static int $_iLogPage = 0;

    /** Write Log to DB */
    protected static ?bool $_bWriteToDb = null;

    /** Maximum Log Entrys in array */
    protected static int $_iMaxLog = 5000;

    /** return the actual log page */
    public static function getLogPage(): int
    {
        return self::$_iLogPage;
    }

    /** get status for write the DB-Log */
    public static function getWriteLogToDbStatus(): bool
    {
        if (is_null(self::$_bWriteToDb)) {
            self::$_bWriteToDb = Registry::getConfig()->getConfigParam('bTRWToolsLogWriteToDb');
        }

        return self::$_bWriteToDb;
    }

    /** write the logs to the DB */
    public static function writeLogToDb(): void
    {
        if (self::getWriteLogToDbStatus()) {
            $oLogger = ToolsMonologDBLogger::getDBLogger();

            foreach (ToolsLog::getLog() as $aParams) {
                $oLogger->{$aParams['level']}($aParams['message']);
            }
        }
    }

    /**
     * load the log from DB.
     *
     * @param int $iPage - Number of logPage
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public static function loadLogFromDb(int $iPage = 0): void
    {
        $iLimit = (int) Registry::getConfig()->getConfigParam('iTRWToolsLogLoadMaxEntries');
        if ($iLimit === 0 || $iLimit > self::$_iMaxLog) {
            $iLimit = self::$_iMaxLog;
            ToolsConfig::saveConfigParam('iTRWToolsLogLoadMaxEntries', $iLimit, 'num', 'trwtools');
        }

        $viewNameGenerator = Registry::get(TableViewNameGenerator::class);
        $sLogView = $viewNameGenerator->getViewName('trwtoolsmonolog');

        $iPos = $iPage * $iLimit;
        $sSelect = "select `oxchannel` as channel, `oxlevel` as level, `oxmessage` as message, `oxdatetime` as datetime
            from {$sLogView}
            order by `oxdatetime` desc
            limit " . $iPos . ', ' . $iLimit;

        $oResults = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->select($sSelect);
        if ($oResults->count() > 0) {
            self::$_iLogPage = $iPage + 1;

            $aResults = [];
            while (!$oResults->EOF) {
                $aResults[] = $oResults->getFields();
                $oResults->fetchRow();
            }
            ToolsLog::setLog($aResults);
        }
    }

    /** clean the log */
    public static function cleanLogFromDb(): void
    {
        $oLogger = ToolsMonologDBLogger::getDBLogger();
        $oLogger->reset();
    }
}
