<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Core;

use OxidEsales\Eshop\Core\Exception\StandardException;

class ExceptionValidator
{
    public function isInvalid(StandardException $exception): bool
    {
        if ($this->isSystemComponentNotFound($exception)) {
            return true;
        }

        if ($this->isLocatorDataNotFound($exception)) {
            return true;
        }

        return false;
    }

    protected function isSystemComponentNotFound(StandardException $exception): bool
    {
        $sExceptionMessage = $exception->getMessage();
        $aStringToClean = [
            'EXCEPTION_SYSTEMCOMPONENT_CLASSNOTFOUND ',
            'ERROR_MESSAGE_SYSTEMCOMPONENT_FUNCTIONNOTFOUND ',
        ];
        $bCheckMessage = false;
        foreach ($aStringToClean as $sStringToClean) {
            if (strpos($sExceptionMessage, $sStringToClean) !== false) {
                $sExceptionMessage = substr($sExceptionMessage, strlen($sStringToClean));
                $bCheckMessage = true;
            }
        }

        if (!$bCheckMessage) {
            return false;
        }

        if (!preg_match('/^[a-zA-Z1-9_]\w*$/i', $sExceptionMessage)) {
            return true;
        }

        return false;
    }

    protected function isLocatorDataNotFound(StandardException $exception): bool
    {
        $sExceptionMessage = $exception->getMessage();

        if (
            preg_match(
                '/Function \'_set(.*)LocatorData\' does not exist or is not accessible!/i',
                $sExceptionMessage
            )
        ) {
            return true;
        }

        return false;
    }
}
