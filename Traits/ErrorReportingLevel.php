<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ToolsModule\Traits;

use OxidEsales\Eshop\Core\Registry;

trait ErrorReportingLevel
{
    protected array $aLogLevels = [
        'default' => E_ALL & ~E_NOTICE,
        'all' => E_ALL,
        'all_no_notice' => E_ALL & ~E_NOTICE,
        'all_no_noticewarning' => E_ALL & ~E_NOTICE & ~E_WARNING,
        'all_no_noticewarningdeprecated' => E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED,
        'just_errors' => E_ERROR,
    ];

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _getErrorReportingLevel() // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        $result = $this->getExtendErrorReportingLevel();
        return false !== $result ? $result : parent::_getErrorReportingLevel();
    }

    protected function getExtendErrorReportingLevel()
    {
        $config = Registry::getConfig();
        $override = $config->getConfigParam('bTRWToolsLoglevelOverride');
        $logLevelOverride = $config->getConfigParam('sTRWToolsLoglevelOverrideDefault');

        if ($override && ini_get('log_errors')) {
            if ($logLevelOverride === "flexible") {
                return $config->getConfigParam('sTRWToolsLoglevelOverrideFlex');
            }
            return $this->aLogLevels[$logLevelOverride];
        }
        return false;
    }
}
